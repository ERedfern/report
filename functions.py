import numpy as np
import parameters as p
import struct
import matplotlib.pyplot as plt
from ipywidgets import interactive

#---------------------------------------------------------------------------
#  Functions for the pseudospectral solution of the Navier-Stokes Equations
#  Parameters shared via the parameters module and p namespace. 
#---------------------------------------------------------------------------
 
def init(Nx,Ny,Lx,Ly,n,dt,Re):
    #-------------------------------------------------------------------------
    # Initialise parameters, wavenumbers, filter, Crank-Nicolson coefficients
    #-------------------------------------------------------------------------

    nu = 1./Re

    # Set up wavenumbers (care needs to be taken here)
    M = int(Nx/2) + 1

    alphx = 2*np.pi/Lx
    alphy = 2*np.pi/Ly

    freq = np.fft.fftfreq(Nx,d=1./Nx)
    kkx = freq[0:M]*alphx
    kky = np.fft.fftfreq(Ny,d=1./Ny)*alphy

    kx,ky = np.meshgrid(kkx,kky)    # 2D wavenumber arrays
    ksq = (kx*kx+ky*ky)             # k^2
    iksq = np.where(np.sqrt(ksq) != 0, 1./(1e-15+ksq), 0)  # k^-2

    # Filter for dealiasing
    p.filtr = np.ones_like(ksq)
    p.filtr[np.where(np.sqrt(ksq)>(Nx//3)-0.5)] = 0.
    p.filtr = np.where((kx==0)&(ky==0),0,p.filtr) # This is required to remove mean flows in the domain
    p.filtr[(Ny//2)-1:0:-1,0] = 0
    
    
    # crank-nicolson factors (C-N is diagonal in spectral space so doesn't need extra solvers)
    p.cn1 = 1./(1+(0.5*ksq*nu*dt))
    p.cn2 = 1-(0.5*ksq*nu*dt)
    
    # forcing term
    p.f1 = np.where((kx==0)&((ky==n)|(ky==-n)),n/2.,0)
    
    # Set the shared variables
    p.kx = kx
    p.ky = ky
    
    p.ksq = ksq
    p.iksq = iksq
    
    p.Nx = Nx
    p.Ny = Ny
    p.N = Nx*Ny
    p.Lx = Lx
    p.Ly = Ly
    p.n = n
    p.dt = dt
    p.Re = Re
    p.dx = Lx/Nx
    p.dy = Ly/Ny
    

    # Set the initial condition (IN SPECTRAL SPACE)
    #-------------------------------------------------------------------------
    w0 = np.where((kx==1)&((ky==1)|(ky==-1)),-0.5+0j,0)
    w0 += np.where((kx==2)&((ky==3)|(ky==4)),-4+1j,0)
    w0 += np.where((kx==3)&((ky==4)|(ky==5)),-2+3j,0)
    w0 += np.where((kx==4)&((ky==5)|(ky==6)),8-6j,0)

    return w0

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def rhs(Z):
    # advection and forcing terms 

    Z[(p.Ny//2)-1:0:-1,0] = np.conj(Z[(p.Ny//2)+1:p.Ny,0])     # enforce conj symmetry
    omega = np.fft.irfft2(Z)*p.N                               # physical vorticity
    uk = (1j*p.ky*Z)*p.iksq                                    # spectral u
    vk = -(1j*p.kx*Z)*p.iksq                                   # spectral v
    uk[(p.Ny//2)-1:0:-1,0] = np.conj(uk[(p.Ny//2)+1:p.Ny,0])   # enforce conj symmetry
    u = np.fft.irfft2(uk)*p.N                                  # physical u
    vk[(p.Ny//2)-1:0:-1,0] = np.conj(vk[(p.Ny//2)+1:p.Ny,0])   # enforce conj symmetry
    v = np.fft.irfft2(vk)*p.N                                  # physical v
    u_omega = u*omega                                          # nonlinear products
    v_omega = v*omega

    # Finally FFT and curl u x omega (this ensures div(u) = 0) and add forcing
    
    curl = (-1j*p.kx*np.fft.rfft2(u_omega)/p.N - 1j*p.ky*np.fft.rfft2(v_omega)/p.N) - p.f1
    return curl

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def rk4(u):
    # Standard RK4, with dealiasing filter
    
    y1 = p.dt*rhs(u)*p.filtr
    y2 = p.dt*rhs(u+0.5*y1)*p.filtr
    y3 = p.dt*rhs(u+0.5*y2)*p.filtr
    y4 = p.dt*rhs(u+y3)*p.filtr
    return ((1./6.)*(y1+(2*y2)+(2*y3)+y4))

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Energy(unew,OnFlag):
    if OnFlag == 1:
        Energy = np.sum(0.5*(p.iksq)*(abs(unew[:,:]))**2) 
    else:
        Energy = 0 
    return Energy

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------
def Diss(unew,Re,OnFlag):
    if OnFlag ==1:
        Diss = 2*(1/Re)*(np.sum(abs(unew[:,:])**2)) 
    else:
        Diss = 0 
    return Diss

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Power(unew,n,OnFlag):
    if OnFlag == 1:
        Nkx,Nky = np.shape(p.kx)
        for i in range(Nkx):
            for j in range(Nky):
                if(p.kx[i,j]==0 and p.ky[i,j]==-n):
                    ifx = i
                    ify = j
    
        imj = unew[ifx,ify].real
        Power = -(1/n)*(imj)
    else:
        Power = 0 
    return Power
   
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Vector(T,dt,unew):
    tmax = int(p.T/p.dt)
    tjump = int(np.ceil(0.1/dt))

    sampletime  = tmax//1
    sampleincs = sampletime//tjump
   
    j=1
    P=1
    tjumpinc = tjump
    Store = np.zeros((sampleincs,np.size(unew[:,0]),np.size(unew[0,:])))
    
    for i in range(sampletime):
        unew = p.cn1*((p.cn2*u) + f.rk4(u)) 
    
        if i ==0:
            Store[0,:,:] = unew[:,:]
    
        if i == tjump:    
        
            Store[P,:,:] = unew[:,:] 
        
        j+=1
        P+=1
        
        tjump = tjump+tjumpinc
        
        u = unew       
        t = t + dt 
     
    Mode = np.ones_like(p.ksq)
    Mode1 = np.ones_like(p.ksq)
    Mode[np.where(np.sqrt(p.ksq)<=1) ] = 0.
    Mode1[np.where(np.sqrt(p.ksq)>5) ] = 0.
    Mode = np.where((p.kx==0)&(p.ky==0),0,Mode)
    Mode1 = np.where((p.kx==0)&(p.ky==0),0,Mode1)
    
    Vec = np.zeros(((sampleincs,np.size(unew[:,0]),np.size(unew[0,:]))))

    for i in range(1,sampleincs):
        Vec[i,:,:] = (Store[i,:,:] - Store[i-1,:,:])*Mode*Mode1
    
    return Vec
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Structure(Nx,Ny):
    
    KTX = Nx//3
    KTY = Nx//3

    IKTX = KTX + 1
    IKTY = KTY + KTY + 1

    istart = 4+8*6
    nBytes = istart + IKTX*IKTY*16 + 4

    #-----------------------

    with open('UPOs_Re60.out', 'rb') as inh:
        indata = inh.read()

    irest = 14

    i = irest*nBytes + 4 + 8*2
    Tend = struct.unpack("d", indata[i:i+8])  

    i = irest*nBytes + 4 + 8*3
    Sx = struct.unpack("d", indata[i:i+8])  

    i = irest*nBytes + 4 + 8*4
    Sy = struct.unpack("d", indata[i:i+8])  

    Zin = np.zeros((IKTX,IKTY),dtype=complex)

    #-----------------------

    for iy in range(IKTY):
        for ix in range(IKTX):
            ik = irest*nBytes + istart + (iy*IKTX + ix)*16
            Z = struct.unpack("dd", indata[ik:ik+16])
            Zin[ix,iy] = complex(Z[0],Z[1])
        
    myZ = np.zeros((Ny,Nx//2+1),dtype=complex) # this is the matrix where I want to dump my z values for IFFT

    for i in range(0,KTY+1):
        myZ[i,0:KTX] = Zin[0:KTX,i+KTY]
    for j in range(KTY):                    
        myZ[Ny-KTY+j,0:KTX] = Zin[0:KTX,j]
    
    return myZ,Tend[0]

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def UPOplot(Y,X,zdata,SaveFlag):
    
    if SaveFlag ==0:
        plt.pcolor(Y*0.1,X*0.1,zdata)
        plt.colorbar(label = 'A(t,T)')
        plt.clim(-1,1)
        plt.ylim(0,50)
        plt.ylabel('-T')
        plt.xlabel('t')
        plt.show()
    else: 
        plt.pcolor(Y*0.1,X*0.1,zdata)
        plt.colorbar(label = 'A(t,T)')
        plt.clim(-1,1)
        plt.ylim(0,50)
        plt.ylabel('-T')
        plt.xlabel('t')
        plt.savefig('mode15.png')
        plt.show()
        
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Residual(Y,X,data,SaveFlag):

    cmap = plt.cm.viridis
    cmap_reversed = plt.cm.get_cmap('viridis_r')
    
    if SaveFlag ==0:
        plt.pcolor(Y*0.2,X*0.2, data, cmap =cmap_reversed)
        plt.colorbar()
        plt.clim(0,1.1)
        plt.ylim(0,50)
        plt.xlabel('$t$', fontsize = 12)
        plt.ylabel('$-T$', fontsize = 12)
        plt.show()
        
    else: 
        plt.pcolor(Y*0.2,X*0.2, data, cmap =cmap_reversed)
        plt.colorbar()
        plt.clim(0,1.1)
        plt.ylim(0,50)
        plt.savefig('angres.pdf')
        plt.xlabel('$t$', fontsize = 12)
        plt.ylabel('$-T$', fontsize = 12)
        plt.show()
        
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def SlidePlot(uplot,Nx,Ny,Lx,Ly,iplot,pstep,t,T,dt):
    
    x = np.linspace(0,Lx-p.dx,Nx)
    y = np.linspace(0,Ly-p.dy,Ny)
    [xx,yy] = np.meshgrid(x,y)

    def plot_slider(t):
        i=int((iplot)*t/T)
        plt.figure(2)
        plt.xlim(0,Lx-p.dx)
        plt.ylim(0,Ly-p.dy)
        plt.ylabel(r'$y$')
        plt.xlabel(r'$x$')
        plt.pcolor(xx,yy,uplot[:,:,i])
        plt.colorbar()
        plt.show()
        return t

    interactive_plot = interactive(plot_slider, t=(0,(iplot-1)*pstep*dt,pstep*dt))
    interactive_plot
    
    return interactive_plot
    
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Dissplot(Time,Dis,dt):
    plt.plot(Time*dt,Dis,'r')           
    plt.xlabel("t") 
    plt.ylabel("$D_t$")
    plt.title("$D_t$ trend in time")
    plt.show()

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------
def Energyplot(Time,Eng,dt):
    plt.plot(Time*dt,Eng,'b')           
    plt.xlabel("t") 
    plt.ylabel("$E_t$")
    plt.title("$E_t$ trend in time")
    plt.show()

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Powerplot(Time,Pow,dt):
    plt.plot(Time*dt,Pow,'g')           
    plt.xlabel("t") 
    plt.ylabel("$I_t$")
    plt.title("$I_t$ trend in time")
    plt.show()

#-------------------------------------------------------------------------
#-------------------------------------------------------------------------

def Phaseplot(Pow,Dis,dlam,tmax):
    plt.plot(Pow[tmax//8:tmax]/dlam,Dis[tmax//8:tmax]/dlam,'g')            
    plt.xlabel("$I/D_{lam}$") 
    plt.ylabel('$D/D_{lam}$')
    plt.ylim()
    plt.xlim()
    plt.title("")
    plt.show()
    
#-------------------------------------------------------------------------
#-------------------------------------------------------------------------







































