import numpy as np

# The repeat below is just an initialisation 
# of dummy variables.

# They can be edited in the notebook which
# will then be updated by init. 

# Forcing wavenumber
n = 0

# Reynolds number
Re = 0.

# Time & final time
t = 0.
T = 0.

#timestep 
dt = 0.
#Resolution
Nx = 0
Ny = 0
N = 0

#Length of domain
Lx = 0.
Ly = 0.

#Spatial difference
dx = 0.
dy = 0.

nu = 0.
alphx = 0.
alphy = 0.

#Physical space domain
x = 0.
y = 0.
[xx,yy] = np.meshgrid(x,y)

M = 0

kx =  0.
ky = 0.
ksq =  0.
iksq =  0.
filtr =  0.
cn1 = 0.
cn2 = 0.
f1 = 0. 

