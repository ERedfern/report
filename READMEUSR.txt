##--- Two dimensional Kolmogorov flow ---##

A psuedo-spectral code implemented on the two dimesnional vorticity equation.

 
FILES

--- functions.py ---

fucntions that are needed for the other codes in here.

--- parameters.py ---

initialises paramters here.

--- Kflow ---

Introduction to implementation of psuedo-spectral code for 2d Kolmogorov flow.

--- forcedphaseshift ---

A snapshot of the flow is moved in the x direction using a rotation in phase. We the calculate the average/modal shift. The error for each forced shift is calculated.

--- UPOcheck/UPOnewres ---

Verifes that direction data can detect periodic behaviour.

--- Wavecreate ---

Creates snapchots of the flow to be saved.

--- NResidual/NResdiual-weight ---

Calculates and plots distance, direction and combines residual.

--- laminarvalues ---

Calculates dissipation for different Reynolds number.



